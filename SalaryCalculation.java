

import java.util.Scanner;

public class SalaryCalculation {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int []array = new int[7];//number of days
        for (int i = 0; i < 7; i++) {
            array[i] = sc.nextInt();//getting input
        }

        double sum = 0;//sum of salary
        int hours = 0;//no_of hours worked
        for (int i = 0; i < 7; i++) {
            sum += (array[i] * 100);
            if (array[i] > 8) {
                sum += (array[i] - 8) * 15;
            }
            hours += array[i];
        }
        if(hours>40)
        {
            sum=sum+(hours-40)*25;
        }
        else
        {
            sum+=(array[0]*100)*0.50;//sunday_bonus
            sum+=(array[6]*100)*0.25;//saturday_bonus
        }

        System.out.println(sum);//total salary

    }
}

